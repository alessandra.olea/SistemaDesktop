/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Utilities;

import java.sql.*;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 *
 * @author aluno
 */
public class BDConnection {
    public Statement stm;//responsável pelas pesquisas na BD
    public ResultSet rs;//armazena o resultado de uma pesquisa passada para o statement
    private String driver = "org.postgresql.Driver";//identifica o serviço de banco de dados
    private String way = "jdbc:postgresql://localhost:5432/SystemClass";//configura o local do BD
    private String user = "postgres";
    private String password = "Aa11502431aA";
    public Connection conn;//conexão com o BD
    
    public void connection(){
        //método responsável pela conexão com o BD
        try {//tentativa inicial
            System.setProperty("jdbc.Drivers", driver);//configura a propriedade do driver de conexão
            
            conn = DriverManager.getConnection(way, user, password);//realiza a conexão
            
            //JOptionPane.showMessageDialog(null, "Conectado com sucesso!");//imprime uma caixa de mensagem
            
        } catch (SQLException ex) {//exceção
            Logger.getLogger(BDConnection.class.getName()).log(Level.SEVERE, null, ex);
            
            JOptionPane.showMessageDialog(null, "Erro de conexão! \n Erro:"+ex.getMessage());
        }
        
    }
    
    public void executeSQL(String sql){
        try {
            stm = conn.createStatement(rs.TYPE_SCROLL_INSENSITIVE,rs.CONCUR_READ_ONLY);
            rs = stm.executeQuery(sql);//recebe a string sql
        } catch (SQLException ex) {
            //JOptionPane.showMessageDialog(null, "Erro de ExecuteSQL! \n ERRO: "+ex.getMessage());
            
        }
    }
    
    public void disconnect(){//método para fechar a conexão com o BD
        
        try {
            conn.close();//fecha a conexão
            JOptionPane.showMessageDialog(null, "Desconectado com sucesso!");
        } catch (SQLException ex) {
            Logger.getLogger(BDConnection.class.getName()).log(Level.SEVERE, null, ex);
            JOptionPane.showMessageDialog(null, "Erro ao fechar a conexão! \n Erro:"+ex.getMessage());
        }
      
        }
    }

